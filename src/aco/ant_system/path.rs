use graph::node::NodeId;

use super::edge::DistanceType;

#[derive(Debug, Default)]
pub struct Path {
    pub nodes: Vec<NodeId>,
    pub total_distance: DistanceType,
}
