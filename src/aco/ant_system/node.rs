pub use graph::node::*;

#[derive(Debug, Default)]
pub struct Node {
    id: NodeId,
    name: String,
}

impl BaseNode for Node {
    fn new(id: NodeId) -> Self {
        Node {
            id,
            name: format!("Node ({id})"),
        }
    }

    fn id(&self) -> NodeId {
        self.id
    }
}

impl NamedNodeTrait for Node {
    fn name(&self) -> &str {
        &self.name
    }

    fn set_name(&mut self, name: &str) {
        self.name = name.to_string();
    }
}
