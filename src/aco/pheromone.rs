pub type PheromoneType = f64;

pub const DEFAULT_AMOUNT: PheromoneType = 0.1;
