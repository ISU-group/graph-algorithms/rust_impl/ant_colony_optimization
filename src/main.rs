use std::{
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

use aco::{
    ant_system::{edge::DistanceType, AntGraph, AntSystem},
    params::{AntPopulation, HeuristicParams},
};
use graph::node::NamedNodeTrait;

mod aco;

fn main() {
    let filepath = "graph.txt";
    let mut ant_graph = read_graph(filepath);

    let heuristic_params = HeuristicParams {
        secretion: 20.0,
        evaporation_rate: 0.6,
        importance: 1.5,
        sensitivity: 4.0,
    };

    let ant_population = AntPopulation {
        workers: 54,
        elite: 10,
    };

    let ant_system = AntSystem::new(heuristic_params, ant_population);
    let path = ant_system.optimal_path(&mut ant_graph, "1", "9");

    let str_path = path
        .nodes
        .iter()
        .map(|n| ant_graph.node_by_id(*n).unwrap().name())
        .collect::<Vec<_>>()
        .join(" -> ");

    println!("Path: {str_path}");
    println!("Total distance: {}", path.total_distance);
}

fn read_graph<P>(filepath: P) -> AntGraph
where
    P: AsRef<Path>,
{
    let file = File::open(filepath).unwrap();
    let reader = BufReader::new(file);

    let mut graph = AntGraph::default();

    for line in reader.lines() {
        let line = line.unwrap();
        let mut split_line = line.split_whitespace();

        if let Some(from) = split_line.next() {
            graph.add_node(from);

            if let Some(to) = split_line.next() {
                graph.add_node(to);

                match split_line.next() {
                    Some(distance) => graph.add_edge(from, to, distance.parse().unwrap()),
                    None => graph.add_edge(from, to, 1 as DistanceType),
                }
                .unwrap();
            }
        }
    }

    graph
}
